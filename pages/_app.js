import { Amplify } from "aws-amplify";
import { Authenticator } from "@aws-amplify/ui-react";
import { MapView } from '@aws-amplify/ui-react';
import { View } from '@aws-amplify/ui-react';
import '@aws-amplify/ui-react/styles.css';
import awsExports from "../src/aws-exports";
import "../styles/globals.css";
import { SelectField } from '@aws-amplify/ui-react';

Amplify.configure({ ...awsExports, ssr: true });
function MyApp({ Component, pageProps }) {
  return (
		<Authenticator loginMechanisms={['username']}>
      {({ signOut, user }) => (
        <main>
          <h1>Hello {user.username}</h1>
          <SelectField label="What is the Fruit?">
            <option value="apple">Apple</option>
            <option value="banana">Banana</option>
            <option value="orange">Orange</option>
          </SelectField>
				  you got a 100 greenhouse score
          <button onClick={signOut}>Sign out</button>
        </main>
      )}
    </Authenticator>
  );
}
export default MyApp;
